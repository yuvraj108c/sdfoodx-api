﻿using Newtonsoft.Json;
using SdFoodxAPI.Data;
using SdFoodxAPI.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SdFoodxAPI.Services
{
    public class NucleusService
    {
        public static EmployeeDTO Login(Auth auth)
        {
            var uri = Appsettings.uri + "/login";
            var httpClient = new HttpClient();
            var postContent = new StringContent(JsonConvert.SerializeObject(auth), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync(uri, postContent).Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                string res_str = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<EmployeeDTO>(res_str);
            }
            else
                return null;
        }

        public static async Task<List<EmployeeDetailDTO>> GetEmployeedetails()
        {
            var uri = Appsettings.uri + "/users";
            var httpClient = new HttpClient();

            var content = await httpClient.GetStringAsync(uri);

            return JsonConvert.DeserializeObject<List<EmployeeDetailDTO>>(content); ;
        }
    }
}