﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;
using SdFoodxAPI.Models;

namespace SdFoodxAPI.Controllers
{
    public class FoodItemsController : ApiController
    {
        private SdFoodxAPIContext db = new SdFoodxAPIContext();
        
        [Authorizer]
        [Authorize( Roles = "canteen,employee")]
        [HttpGet]
        public HttpResponseMessage GetAllFood()
        {
            int supplier_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);

            var query = db.FoodItems
               .Join(db.categories, fi => fi.category_id, c => c.category_id, (fi, c)
               => new { FoodItem = fi, Category = c })
               .Where(a => a.FoodItem.supplier_id == supplier_id)
               .Select(s => new
               {
                   s.FoodItem.food_id, s.FoodItem.food_name,s.FoodItem.food_description, s.FoodItem.food_price, s.FoodItem.food_limit, s.Category.category_id, s.Category.category_name, s.FoodItem.isVeg, s.FoodItem.food_rating, s.FoodItem.food_image
                  
               });

            return Request.CreateResponse(HttpStatusCode.OK, query);
        }

        [Authorizer]
        [Authorize(Roles = "canteen")]
        [HttpPut]
        // PUT: api/FoodItems/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFoodItem(FoodItem foodItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(foodItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodItemExists(foodItem.food_id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(foodItem);
        }

        [Authorizer]
        [Authorize(Roles = "canteen")]
        [HttpPost]
        public HttpResponseMessage PostFood([FromBody] FoodItem foodItem)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                
                    db.FoodItems.Add(foodItem);
                    db.SaveChanges();
                    
                    transaction.Commit();
                    return Request.CreateResponse(HttpStatusCode.OK,foodItem);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    return Request.CreateResponse(HttpStatusCode.Conflict);
                }
            }


        }


        [Authorizer]
        [Authorize(Roles = "canteen")]
        [HttpDelete]
        // DELETE: api/FoodItems/5
        [ResponseType(typeof(FoodItem))]
        public IHttpActionResult DeleteFoodItem(int id)
        {
            FoodItem foodItem = db.FoodItems.Find(id);
            if (foodItem == null)
            {
                return NotFound();
            }

            db.FoodItems.Remove(foodItem);
            db.SaveChanges();

            return Ok(foodItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FoodItemExists(int id)
        {
            return db.FoodItems.Count(e => e.food_id == id) > 0;
        }
    }
}