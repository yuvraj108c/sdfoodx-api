﻿namespace SdFoodxAPI.Controllers
{
    using Microsoft.IdentityModel.Tokens;
    using SdFoodxAPI.Data;
    using SdFoodxAPI.Models;
    using SdFoodxAPI.Services;
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;

    [Route("api/auth")]
    public class AuthsController : ApiController
    {
        private SdFoodxAPIContext db = new SdFoodxAPIContext();

        // POST: api/Auth
        [HttpPost]
        public async Task<IHttpActionResult> Login(Auth auth)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }

            EmployeeDTO employee = NucleusService.Login(auth);

            if (employee != null)
            {
                var claims = CreateClaims(employee.id, employee.name, "employee", 0); // TODO: Get Role  //supplierId=0 because we do not need it. 0 is just a useless number
                var jwt_token = CreateJWToken(claims);

                var response = new { token = new JwtSecurityTokenHandler().WriteToken(jwt_token), id = employee.id, name = employee.name, role = "employee", supplierid = 0 };//TODO: Get role
                return Ok(response);
            }
            else
            {
                var outsideUser = db.OutsideUsers.Where(a => a.email == auth.email && a.password == auth.password).FirstOrDefault();

                if (outsideUser != null)
                {
                    var claims = CreateClaims(outsideUser.id, outsideUser.name, "Canteen", outsideUser.supplier_id);
                    var jwt_token = CreateJWToken(claims);

                    var response = new { token = new JwtSecurityTokenHandler().WriteToken(jwt_token), id = outsideUser.id, name = outsideUser.name, email = outsideUser.email, role = "canteen" , supplierid= outsideUser.supplier_id };
                    return Ok(response);
                }
                else
                {
                    return BadRequest("Invalid Credentials");
                }
            }
        }

        private Claim[] CreateClaims(int sid, String name, String role, int supplierId)
        {
            Claim[] claims = {
                    new Claim(ClaimTypes.Sid, sid.ToString()),
                    new Claim(ClaimTypes.Name, name),
                    new Claim(ClaimTypes.Role, role),
                    new Claim("supplierId", supplierId.ToString()),
                };

            return claims;
        }

        private JwtSecurityToken CreateJWToken(Claim[] claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Appsettings.secret));
            SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwt_token = new JwtSecurityToken("SdWorx", "SdFoodX", claims, expires: DateTime.UtcNow.AddDays(10), signingCredentials: signIn);

            return jwt_token;
        }
    }
}
