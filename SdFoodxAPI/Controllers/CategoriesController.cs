﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SdFoodxAPI.Models;

namespace SdFoodxAPI.Controllers
{
    public class CategoriesController : ApiController
    {
        private SdFoodxAPIContext db = new SdFoodxAPIContext();

        // GET: api/Categories
        [Authorizer]
        [Authorize(Roles = "canteen,employee")]
        public IQueryable<Category> Getcategories()
        {
            if (Thread.CurrentPrincipal.IsInRole("employee"))
            {
                return db.categories;
            }
            else
            {
                int supplier_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);
                return db.categories.Where(c => c.supplier_id == supplier_id);
            }
        }


        // GET: api/Categories/5
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            Category category = await db.categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }


        // POST: api/Categories
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> PostCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.categories.Add(category);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = category.category_id }, category);
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> DeleteCategory(int id)
        {
            Category category = await db.categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            db.categories.Remove(category);
            await db.SaveChangesAsync();

            return Ok(category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.categories.Count(e => e.category_id == id) > 0;
        }
    }
}