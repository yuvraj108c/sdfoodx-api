﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;
using SdFoodxAPI.Models;

namespace SdFoodxAPI.Controllers
{
    public class SchedulesController : ApiController
    {
        private SdFoodxAPIContext db = new SdFoodxAPIContext();

        [Authorizer]
        [Authorize(Roles = "employee,canteen")]
        [HttpGet]
        public HttpResponseMessage GetAllSchedules()
        {
            if (Thread.CurrentPrincipal.IsInRole("employee"))
            {
                var query = db.schedules
                   .Join(db.FoodItems, s => s.food_id, fi => fi.food_id, (s, fi)
                   => new { Schedule = s, FoodItem = fi })
                   .Join(db.suppliers, fi => fi.FoodItem.supplier_id , supp => supp.supplier_id, (fi, supp)
                   => new { Schedule = fi.Schedule, FoodItem = fi.FoodItem, Supplier = supp })
                   .Select(s => new
                   {
                       s.FoodItem.food_id,
                       s.FoodItem.food_name,
                       s.FoodItem.food_description,
                       s.FoodItem.food_price,
                       s.FoodItem.category_id,
                       s.FoodItem.food_limit,
                       s.FoodItem.isVeg,
                       s.FoodItem.food_image,
                       s.FoodItem.food_rating,
                       s.Supplier.supplier_name,
                       day = s.Schedule.day.ToString(),
                       day_id = s.Schedule.day
                   });
                return Request.CreateResponse(HttpStatusCode.OK, query);
            }
            else
            {
                int supplier_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);

                var query = db.schedules
                   .Join(db.FoodItems, s => s.food_id, fi => fi.food_id, (s, fi)
                   => new { Schedule = s, FoodItem = fi })
                   .Where(a => a.FoodItem.supplier_id == supplier_id)
                   .Select(s => new
                   {
                       s.FoodItem.food_id,
                       s.FoodItem.food_name,
                       s.FoodItem.food_description,
                       s.FoodItem.food_price,
                       s.FoodItem.category_id,
                       s.FoodItem.food_limit,
                       s.FoodItem.isVeg,
                       s.FoodItem.food_image,
                       s.FoodItem.food_rating,
                       day = s.Schedule.day.ToString(),
                       day_id = s.Schedule.day

                   });
                return Request.CreateResponse(HttpStatusCode.OK, query);
            }                    
        }

        // POST: api/Schedules
        [Authorizer]
        [Authorize(Roles = "canteen")]
        [HttpPost]
        public IHttpActionResult PostSchedule(List<Schedule> schedules)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (var schedule in schedules)
                    {
                        db.schedules.Add(schedule);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return Ok("Schedule created ");
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }


        }

        //// PUT: api/Schedules/5
        //[Authorizer]
        //[Authorize(Roles = "canteen")]
        //[HttpPut]
        //public IHttpActionResult PatchSchedule(int id, [FromBody] Schedule schedule)
        //{
        //    try
        //    {
        //        var sc = db.schedules.Where(x => x.food_id == schedule.food_id && x.day == schedule.day).FirstOrDefault();

        //        if (sc != null)
        //        {
        //            sc.day = (DayType) id;
        //            db.SaveChanges();
        //            return Ok("schedule updated sucessfully");
        //        }
        //        else
        //        {
        //            return BadRequest("invalid schedule");
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        return BadRequest(e.Message);
        //    }
        //}

        // DELETE: api/Schedules
        [Authorizer]
        [Authorize(Roles = "canteen")]
        [HttpDelete]
        public IHttpActionResult DeleteSchedule([FromBody] List<Schedule> schedules)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (var schedule in schedules)
                    {
                        Schedule sc = db.schedules.FirstOrDefault(s => s.food_id == schedule.food_id && s.day == schedule.day);

                        if (sc == null)
                        {
                            transaction.Rollback();
                            return NotFound();
                        }

                        db.schedules.Remove(sc);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return Ok("Schedules removed");
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }

        }

    }
}