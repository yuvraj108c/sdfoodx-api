﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SdFoodxAPI.Data;
using SdFoodxAPI.Models;
using SdFoodxAPI.Services;

namespace SdFoodxAPI.Controllers
{
    public class SpecialRequestsController : ApiController
    {
        private SdFoodxAPIContext db = new SdFoodxAPIContext();

        [Authorizer]
        [Authorize(Roles = "employee,canteen")]
        public async Task<HttpResponseMessage> GetRequests()
        {
            if (Thread.CurrentPrincipal.IsInRole("employee"))
            {
                //returns only requests where a suplier has approved 
                int emp_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);
                var query = db.specialRequests.Where(sr => sr.emp_id == emp_id && sr.request_date >= DateTime.Now)
                            .Join(db.suppliers, sr => sr.supplier_id, ssr => ssr.supplier_id, (sr, ssr)
                             => new { SR = sr, Supplier = ssr })
                            .DefaultIfEmpty()
                            .Select(s => new
                            {
                                s.SR.special_id,
                                s.SR.request_date,
                                s.SR.comment,
                                s.SR.description,
                                s.SR.emp_id,
                                s.SR.total_price,
                                s.SR.status,
                                s.SR.supplier_id,
                                s.Supplier.supplier_name
                            });


                return Request.CreateResponse(HttpStatusCode.OK, query);

            }else if (Thread.CurrentPrincipal.IsInRole("canteen"))
            {
                int supplier_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);

                List<EmployeeDetailDTO> users = await NucleusService.GetEmployeedetails();
                DateTime today = DateTime.Now;
                today = today.AddDays(-1);

                var result = users.Join(db.specialRequests.Where(sr=> sr.supplier_id == null || sr.supplier_id == supplier_id), u => u.user_id, sr => sr.emp_id, (u,sr) => new { User= u, SR= sr})
                            .Where(g => g.SR.request_date >= today)
                            .Select(f => new
                            {
                                emp_name= f.User.user_name,
                                f.SR.emp_id,
                                f.SR.special_id,
                                f.SR.description,
                                f.SR.comment,
                                f.SR.request_date,
                                f.SR.total_price,
                                f.SR.status
                            });

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Authorisation denied");
        }

        [Authorizer]
        [Authorize(Roles = "employee")]
        // POST: api/SpecialRequests
        [ResponseType(typeof(SpecialRequest))]
        public async Task<IHttpActionResult> PostSpecialRequest(SpecialRequest specialRequest)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            specialRequest.emp_id = int.Parse(Thread.CurrentPrincipal.Identity.Name); 
            db.specialRequests.Add(specialRequest);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = specialRequest.special_id }, specialRequest);
        }

        [Authorizer]
        [Authorize(Roles = "canteen")]
        [HttpPatch]
        public IHttpActionResult PatchSpecialRequest([FromBody] SpecialRequest sp)
        {
            try
            {
                var y = db.specialRequests.Where(sr => sr.special_id == sp.special_id).FirstOrDefault();

                if (y != null)
                {
                    int supplier_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);

                    if (sp.status > 0) y.status = sp.status;
                    if(sp.comment !=null) y.comment = sp.comment;
                    if(sp.total_price > 0) y.total_price = sp.total_price;
                    y.supplier_id = supplier_id;
                    db.SaveChanges();
                    return Ok("Updated successfully");
                }
                else 
                {
                    return BadRequest("Invalid special request");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        // DELETE: api/SpecialRequests/5
        [Authorizer]
        [Authorize(Roles = "employee")]
        [ResponseType(typeof(SpecialRequest))]
        public IHttpActionResult DeleteSpecialRequest(int id)
        {
            SpecialRequest specialRequest = db.specialRequests.Find(id);
            if (specialRequest == null)
            {
                return NotFound();
            }

            if(specialRequest.status == OrderStatus.PENDING)
            {
                db.specialRequests.Remove(specialRequest);
                db.SaveChanges();
                return Ok(specialRequest);
            }
            else
            {
                return BadRequest("Cannot delete special request.");
            }

        }

    }
}