﻿namespace SdFoodxAPI.Controllers
{
    using SdFoodxAPI.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Web.Http;
    using System.Data.Entity;
    using System.Threading.Tasks;
    using SdFoodxAPI.Services;
    using SdFoodxAPI.Data;

    public class OrdersController : ApiController
    {
        private SdFoodxAPIContext db = new SdFoodxAPIContext();


        [Authorizer]
        [Authorize(Roles = "employee,canteen")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetOrders()
        {
            if (Thread.CurrentPrincipal.IsInRole("employee"))
            {
                int emp_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);

                var query = db.orders.Where(o => o.emp_id == emp_id)
                                     .GroupJoin(db.orderDetails,
                                        o => o.order_id, od => od.order_id,
                                        (o, od) => new { OrderDetail = od, Order = o })
                                        .Select(x => new
                                        {
                                            x.Order.order_id,
                                            x.Order.timestamp_ordered,
                                            x.Order.total_price,
                                            x.Order.emp_id,
                                            food_items = x.OrderDetail.Join(db.FoodItems,
                                                                        od => od.food_id, fi => fi.food_id,
                                                                        (od, fi) => new { OrderDetail = od, FoodItem = fi })
                                                                                .Select(y => new { y.FoodItem, y.OrderDetail })
                                                                                .Join(db.categories, fi => fi.FoodItem.category_id, c => c.category_id
                                                                                , (fi, c) => new { FoodItem = fi, Category = c, OrderDetail = fi.OrderDetail })
                                                                                //.Where(g => g.OrderDetail.orderdate >= System.DateTime.Now)
                                                                                .Where(g => (DbFunctions.DiffDays(DateTime.Now, g.OrderDetail.orderdate) >= 0))
                                                                                .Select(f => new
                                                                                {
                                                                                    f.FoodItem.FoodItem.food_id,
                                                                                    f.FoodItem.FoodItem.food_name,
                                                                                    f.FoodItem.FoodItem.food_description,
                                                                                    f.FoodItem.FoodItem.food_price,
                                                                                    f.FoodItem.FoodItem.food_image,
                                                                                    f.FoodItem.FoodItem.isVeg,
                                                                                    f.Category.category_id,
                                                                                    f.Category.category_name,
                                                                                    f.OrderDetail.status,
                                                                                    f.OrderDetail.quantity,
                                                                                    f.OrderDetail.comment,
                                                                                    f.OrderDetail.orderdate
                                                                                })
                                        });

                return Request.CreateResponse(HttpStatusCode.OK, query);
            }
            else
            {
                int supplier_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);

                List<EmployeeDetailDTO> users = await NucleusService.GetEmployeedetails();

                DateTime today = DateTime.Now;
                today = today.AddDays(-1);
                //bsn order by date 
                var temp = users.Join(db.orders, u => u.user_id, o => o.emp_id, (u, o) => new { User = u, Order = o }).Select(x=> new { x.User.user_id, x.User.user_name, x.Order}).ToList();
                var query = temp
                    .GroupJoin(db.orderDetails,
                                    o => o.Order.order_id, od => od.order_id,
                                    (o, od) => new { OrderDetail = od, Order = o })
                                    .Select(x => new
                                    {
                                        x.Order.user_name,
                                        x.Order.Order.order_id,
                                        x.Order.Order.timestamp_ordered,
                                        x.Order.Order.total_price,
                                        x.Order.Order.emp_id,
                                        food_items = x.OrderDetail.Join(db.FoodItems.Where(g=>g.supplier_id == supplier_id),
                                                             od => od.food_id, fi => fi.food_id,
                                                                    (od, fi) => new { OrderDetail = od, FoodItem = fi })
                                                                            .Select(y => new { y.FoodItem, y.OrderDetail })
                                                                            .Join(db.categories, fi => fi.FoodItem.category_id, c => c.category_id
                                                                            , (fi, c) => new { FoodItem = fi, Category = c, OrderDetails = fi.OrderDetail })
                                                                            .Where(g => (g.OrderDetails.orderdate >= today ))
                                                                            .Select(f => new
                                                                            {
                                                                                f.FoodItem.FoodItem.food_id,
                                                                                f.FoodItem.FoodItem.food_name,
                                                                                f.FoodItem.FoodItem.food_description,
                                                                                f.FoodItem.FoodItem.food_price,
                                                                                f.FoodItem.FoodItem.food_image,
                                                                                f.FoodItem.FoodItem.isVeg,
                                                                                f.Category.category_id,
                                                                                f.Category.category_name,
                                                                                f.OrderDetails.status,
                                                                                f.OrderDetails.quantity,
                                                                                f.OrderDetails.comment,
                                                                                f.OrderDetails.orderdate
                                                                            })
                                    });
                return Request.CreateResponse(HttpStatusCode.OK, query);          
            }
        }

        [Authorizer]
        [Authorize(Roles = "employee")]
        [HttpPost]
        public IHttpActionResult PostOrder([FromBody] List<OrderDetail> orders)
        {

            if (orders.Count() == 0)
            {
                return BadRequest("No orders sent");
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    Order o = new Order();
                    o.emp_id = int.Parse(Thread.CurrentPrincipal.Identity.Name);
                    o.timestamp_ordered = System.DateTime.Now;
                    db.orders.Add(o);
                    db.SaveChanges();

                    foreach (OrderDetail od in orders)
                    {
                        od.order_id = o.order_id;
                        od.status = OrderStatus.PENDING;
                        db.orderDetails.Add(od);
                    }

                    db.SaveChanges();
                    transaction.Commit();

                    return Ok(new { order_id = o.order_id, food_items = db.orderDetails.Where(od => od.order_id == o.order_id).Select(x => x.food_id) });
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }
        }

        [Authorizer]
        [Authorize(Roles = "employee,canteen")]
        [HttpPatch]
        public IHttpActionResult PatchOrder([FromBody] OrderDetail order)
        {
            try
            {
                var od = db.orderDetails.Where(o => o.order_id == order.order_id && o.food_id == order.food_id && o.orderdate == order.orderdate).FirstOrDefault();

                if (od != null)
                {
                    if (Thread.CurrentPrincipal.IsInRole("employee"))
                    {
                        if(order.quantity > 0)
                        {
                            od.quantity = order.quantity;
                            db.SaveChanges();
                            return Ok("Updated quantity successfully");
                        }
                        else
                        {
                            return BadRequest("Invalid quantity");
                        }
                    }
                    else if (Thread.CurrentPrincipal.IsInRole("canteen"))
                    {
                         if (Enum.IsDefined(typeof(OrderStatus), order.status) == true)
                        {
                            //if (order.comment != null)
                            //{
                            //    od.comment = order.comment;
                            //}
                            //else
                            //{
                            //    od.status = (OrderStatus)order.status;
                            //}
                            //
                            od.comment = order.comment ?? od.comment;
                            od.status = (order.comment==null)? (OrderStatus)order.status : od.status;
                            //

                            db.SaveChanges();
                            return Ok("Updated order successfully");
                        }
                        else
                        {
                            return BadRequest("Invalid status");
                        }
                    }

                    return BadRequest("Invalid role");
                }
                else
                {
                    return BadRequest("Invalid Order");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [Authorizer]
        [Authorize(Roles = "employee")]
        [HttpDelete]
        public IHttpActionResult DeleteOrder([FromBody] List<OrderDetail> orders)
        {
            HashSet<int> orderIDs = new HashSet<int>();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (var order in orders)
                    {
                        orderIDs.Add(order.order_id);

                        // Delete order detail
                        var od = db.orderDetails.Where(x => x.order_id == order.order_id && x.food_id == order.food_id).FirstOrDefault();

                        if(od.status != OrderStatus.PENDING)
                        {
                            transaction.Rollback();
                            return BadRequest("Order with id " + od.order_id + " and food id " + od.food_id + " cannot be deleted");
                        }
                        db.orderDetails.Remove(od);
                        db.SaveChanges();

                    }

                    foreach(var id in orderIDs)
                    {
                        // find order of deleted order details
                        if (db.orderDetails.Where(odd => odd.order_id == id).FirstOrDefault() == null)
                        {
                            var rem_order = db.orders.Where(o => o.order_id == id).FirstOrDefault();
                            db.orders.Remove(rem_order);
                            db.SaveChanges();
                        }
                    }

                    transaction.Commit();
                    return Ok("Order deleted successfully");
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }

        }
    
    }
}
