﻿namespace SdFoodxAPI.Controllers
{
    using Microsoft.IdentityModel.Tokens;
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    public class Authorizer : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                if (actionContext.Request.Headers == null || actionContext.Request.Headers.Authorization == null)
                    throw new Exception();

                var encodedToken = actionContext.Request.Headers.Authorization.Parameter;
                // todo: validate token first
                var handler = new JwtSecurityTokenHandler();
                var validationParameters = GetValidationParameters();
                SecurityToken validatedToken;
                handler.ValidateToken(encodedToken, validationParameters, out validatedToken);

                // this part is executed iff token is valid
                var jsonToken = handler.ReadToken(encodedToken);

                var tokenStream = handler.ReadToken(encodedToken) as JwtSecurityToken;
                string id = tokenStream.Claims.First<Claim>(c => c.Type == ClaimTypes.Sid).Value;
                string role = tokenStream.Claims.First<Claim>(c => c.Type == ClaimTypes.Role).Value;
                string name = tokenStream.Claims.First<Claim>(c => c.Type == ClaimTypes.Name).Value;
                string supplier_id = tokenStream.Claims.First<Claim>(c => c.Type == "supplierId").Value;
                System.Diagnostics.Debug.WriteLine("wy" + supplier_id);
                System.Diagnostics.Debug.WriteLine("yy" + role);
                System.Diagnostics.Debug.WriteLine("xy" + name);

                //IPrincipal principal = new GenericPrincipal(new GenericIdentity(id), new string[] { name, role, supplier_id });
                IPrincipal principal = (supplier_id == "0") ? new GenericPrincipal(new GenericIdentity(id), new string[] { name, role }) : new GenericPrincipal(new GenericIdentity(supplier_id), new string[] { name, role });
                SetPrincipal(principal);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        public void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }

        private static TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                // todo: move to separate file
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidIssuer = "SdWorx",
                ValidAudience = "SdFoodX",
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Appsettings.secret)) // The same key as the one that generate the token
            };
        }
    }
}
