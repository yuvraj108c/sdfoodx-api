﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class OutsideUser
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phone_num { get; set; }
        public int supplier_id { get; set; }
        public Supplier supplier { get; set; }

    }
}