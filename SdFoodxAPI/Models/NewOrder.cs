﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class NewOrder
    {
        public int quantity { get; set; }
        public int food_id { get; set; }
        public DateTime orderdate { get; set; }
    }
}