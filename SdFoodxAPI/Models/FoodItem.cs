﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SdFoodxAPI.Models
{
    public class FoodItem
    {
        [Key]
        public int food_id { get; set; }
        public string food_name { get; set; }
        public string food_description { get; set; }
        public float food_price { get; set; }
        public string food_image { get; set; }
        public int food_limit { get; set; }
        public int category_id { get; set; }

        [JsonIgnore]
        public Category Categories { get; set; }

        public Boolean isVeg { get; set; }

        public int? food_rating { get; set; }

        public int supplier_id { get; set; } //add foreign key later

        [JsonIgnore]
        public ICollection<OrderDetail> Order_Details { get; set; }
        [JsonIgnore]
        public ICollection<Schedule> schedules { get; set; }

    }
}