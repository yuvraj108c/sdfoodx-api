﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class SpecialRequest
    {
        [Key]
        public int special_id { get; set; }
        public DateTime request_date { get; set; }
        public string comment { get; set; }
        public string description { get; set; }
        public int emp_id { get; set; }
        public float total_price { get; set; }
        public OrderStatus status { get; set; }
        public int? supplier_id { get; set; } //the person who will take that special request

    }
}