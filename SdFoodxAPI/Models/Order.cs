﻿using Newtonsoft.Json;
using SdFoodxAPI.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class Order
    {
        [Key]
        public int order_id { get; set; }
        public DateTime timestamp_ordered { get; set; }
        public float total_price { get; set; }
        public int emp_id { get; set; }
        [JsonIgnore]
        public ICollection<OrderDetail> Order_Details { get; set; }

        internal IEnumerable<object> Join(List<EmployeeDTO> users, Func<object, object> p1, Func<EmployeeDTO, int> p2, Func<object, object, object> p3)
        {
            throw new NotImplementedException();
        }
    }
}