﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SdFoodxAPI.Models
{
    public class Auth
    {
        [Key]
        [Required]
        public string email { get; set; }
        [Required]
        public string password { get; set; }
        public int app_id { get; set; }
    }
}