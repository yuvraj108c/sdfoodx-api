﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class Schedule
    {
        [Key]
        [Column(Order = 0)]
        public int food_id { get; set; }
        public FoodItem Food_Items { get; set; }

        [Key]
        [Column(Order = 1)]
        public DayType day { get; set; }

    }

    public enum DayType
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Everyday
    }
}