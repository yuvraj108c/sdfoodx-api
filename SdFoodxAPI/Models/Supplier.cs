﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class Supplier
    {
        [Key]
        public int supplier_id { get; set; }
        [Required]
        public string supplier_name { get; set; }

        [JsonIgnore]
        public ICollection<OutsideUser> outside_users { get; set; }
    }
}