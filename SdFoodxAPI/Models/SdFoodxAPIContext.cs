﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class SdFoodxAPIContext : DbContext
    {

        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public SdFoodxAPIContext() : base("name=SdFoodxAPIContext")
        { }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.OutsideUser> OutsideUsers { get; set; }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.Supplier> suppliers { get; set; }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.FoodItem> FoodItems { get; set; }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.Category> categories { get; set; }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.Order> orders { get; set; }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.OrderDetail> orderDetails { get; set; }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.Schedule> schedules { get; set; }
        public System.Data.Entity.DbSet<SdFoodxAPI.Models.SpecialRequest> specialRequests { get; set; }
    }

}