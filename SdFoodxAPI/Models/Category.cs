﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class Category
    {
        [Key]
        public int category_id { get; set; }
        [Required]
        public string category_name { get; set; }
        public int supplier_id { get; set; } //add foreign key later

        [JsonIgnore]
        public ICollection<FoodItem> FoodItem { get; set; }

    }
}