﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SdFoodxAPI.Models
{
    public class OrderDetail
    {
        [Key]
        [Column(Order = 0)]
        public int order_id { get; set; }
        public Order Orders { get; set; }
        [Key]
        [Column(Order = 1)]
        public int food_id { get; set; }
        public FoodItem Food_Items { get; set; }
        public int quantity { get; set; }
        public String comment { get; set; }
        [Key]
        [Column(Order = 2)]
        public DateTime orderdate { get; set; }

        public OrderStatus status { get; set; }
    }

    [Flags]
    public enum OrderStatus{
        PENDING,
        APPROVED,
        CANCELLED,
        COMPLETED
    }
}