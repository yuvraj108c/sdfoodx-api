﻿using System;

namespace SdFoodxAPI.Data
{
    public class EmployeeDTO
    {
        public int id { set; get; }

        public string name { get; set; }
        public String role { set; get; }
        public String token { set; get; }

        public int role_id { set; get; }

    }
}