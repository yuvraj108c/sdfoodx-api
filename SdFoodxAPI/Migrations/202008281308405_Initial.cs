﻿namespace SdFoodxAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        category_id = c.Int(nullable: false, identity: true),
                        category_name = c.String(nullable: false),
                        supplier_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.category_id);
            
            CreateTable(
                "dbo.FoodItems",
                c => new
                    {
                        food_id = c.Int(nullable: false, identity: true),
                        food_name = c.String(),
                        food_description = c.String(),
                        food_price = c.Single(nullable: false),
                        food_image = c.String(),
                        food_limit = c.Int(nullable: false),
                        category_id = c.Int(nullable: false),
                        isVeg = c.Boolean(nullable: false),
                        food_rating = c.Int(),
                        supplier_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.food_id)
                .ForeignKey("dbo.Categories", t => t.category_id, cascadeDelete: true)
                .Index(t => t.category_id);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        order_id = c.Int(nullable: false),
                        food_id = c.Int(nullable: false),
                        orderdate = c.DateTime(nullable: false),
                        quantity = c.Int(nullable: false),
                        comment = c.String(),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.order_id, t.food_id, t.orderdate })
                .ForeignKey("dbo.FoodItems", t => t.food_id, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.order_id, cascadeDelete: true)
                .Index(t => t.order_id)
                .Index(t => t.food_id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        order_id = c.Int(nullable: false, identity: true),
                        timestamp_ordered = c.DateTime(nullable: false),
                        total_price = c.Single(nullable: false),
                        emp_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.order_id);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        food_id = c.Int(nullable: false),
                        day = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.food_id, t.day })
                .ForeignKey("dbo.FoodItems", t => t.food_id, cascadeDelete: true)
                .Index(t => t.food_id);
            
            CreateTable(
                "dbo.OutsideUsers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        email = c.String(),
                        password = c.String(),
                        phone_num = c.String(),
                        supplier_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Suppliers", t => t.supplier_id, cascadeDelete: true)
                .Index(t => t.supplier_id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        supplier_id = c.Int(nullable: false, identity: true),
                        supplier_name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.supplier_id);
            
            CreateTable(
                "dbo.SpecialRequests",
                c => new
                    {
                        special_id = c.Int(nullable: false, identity: true),
                        request_date = c.DateTime(nullable: false),
                        comment = c.String(),
                        description = c.String(),
                        emp_id = c.Int(nullable: false),
                        total_price = c.Single(nullable: false),
                        status = c.Int(nullable: false),
                        supplier_id = c.Int(),
                    })
                .PrimaryKey(t => t.special_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OutsideUsers", "supplier_id", "dbo.Suppliers");
            DropForeignKey("dbo.Schedules", "food_id", "dbo.FoodItems");
            DropForeignKey("dbo.OrderDetails", "order_id", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "food_id", "dbo.FoodItems");
            DropForeignKey("dbo.FoodItems", "category_id", "dbo.Categories");
            DropIndex("dbo.OutsideUsers", new[] { "supplier_id" });
            DropIndex("dbo.Schedules", new[] { "food_id" });
            DropIndex("dbo.OrderDetails", new[] { "food_id" });
            DropIndex("dbo.OrderDetails", new[] { "order_id" });
            DropIndex("dbo.FoodItems", new[] { "category_id" });
            DropTable("dbo.SpecialRequests");
            DropTable("dbo.Suppliers");
            DropTable("dbo.OutsideUsers");
            DropTable("dbo.Schedules");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.FoodItems");
            DropTable("dbo.Categories");
        }
    }
}
